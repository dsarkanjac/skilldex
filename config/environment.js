/* jshint node: true */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'skilldex',
    environment: environment,
    baseURL: '/skilldex/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    ENV.baseURL = '/';
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.torii = {
      providers: {
        'google-oauth2-bearer': {
          // remoteServiceName: 'iframe',
          apiKey: '1072391717445-jv29cq4593jf9q0m04s26r90gu4b6tf0.apps.googleusercontent.com',
          redirectUri: 'http://localhost:9000/home'
        }
      }
    };
  }

  if (environment === 'panda') {
    ENV['ember-cli-mirage'] = {
      enabled: true
    };
    ENV.torii = {
      providers: {
        'google-oauth2-bearer': {
          // remoteServiceName: 'iframe',
          apiKey: '1072391717445-bnkkmafu2bptkn6dm9sg41nujnsl1phv.apps.googleusercontent.com',
          redirectUri: 'http://fuzzy-panda.com/skilldex/home'
        }
      }
    };
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.baseURL = '/';
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }
  if (environment === 'production') {
    ENV['ember-cli-mirage'] = {
      enabled: true
    };
    ENV.torii = {
      providers: {
        'google-oauth2-bearer': {
          // remoteServiceName: 'iframe',
          apiKey: '1072391717445-v87ov7p10o1rkna4sccuj0cnlt6jbpvr.apps.googleusercontent.com',
          redirectUri: 'http://office.tmns.rs:81/skilldex/home'
        }
      }
    };
  }
  ENV['ember-simple-auth'] = {
    routeAfterAuthentication: 'home',
    routeIfAlreadyAuthenticated: 'home'
  };

  return ENV;
};
