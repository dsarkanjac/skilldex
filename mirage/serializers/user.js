import BaseSerializer from './application';

export default BaseSerializer.extend({
  include: ['userSkills', 'userEducations', 'userRoles']
});
