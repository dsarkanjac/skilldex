export default function(server) {

  /*
    Seed your development database using your factories.
    This data will not be loaded in your tests.

    Make sure to define a factory for each model you want to create.
  */

  // Create roles.
  const adminRole = server.create('role', {
    'name': 'Administrator'
  });
  const managerRole = server.create('role', {
    'name': 'Manager'
  });
  const employeeRole = server.create('role', {
    'name': 'Employee'
  });
  const candidateRole = server.create('role', {
    'name': null
  });

  for (let i = 0; i < 10; i++) {
    let generatedUserSkills = [];
    let generatedUserEducations = [];
    for (let i = 0; i < 5; i++) {
      generatedUserSkills.push(server.create('user-skill', {
        skill: server.create('skill')
      }));
    }
    for (let i = 0; i < 2; i++) {
      generatedUserEducations.push(server.create('user-education', {
        education: server.create('education')
      }));
    }
    for (let i = 0; i < 2; i++) {
      server.create('education');
    }
    let currentRoles = [server.create('user-role', {
      role: employeeRole
    })];
    if (i === 0) {
      currentRoles.push(server.create('user-role', {
        role: adminRole
      }));
    }
    if (i % 2) {
      currentRoles.push(server.create('user-role', {
        role: managerRole
      }));
    }
    if (i % 3 === 0) {
      currentRoles = [];
    }
    server.create('user', {
      'userSkills': generatedUserSkills,
      'userEducations': generatedUserEducations,
      'userRoles': currentRoles,
      'userWexperiences': server.createList('user-wexperience', Math.floor((Math.random() * 4) + 1)),
      'userOther': server.createList('user-other', Math.floor((Math.random()) + 1))
    });
  }
  server.createList('top-skill', 7);

}
