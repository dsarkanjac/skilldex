/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  newUserExperience: null,
  didValidate: false,
  init() {
    this._super(...arguments);
    this.set('newUserExperience', this.get('store').createRecord('user-wexperience'));
  },
  actions: {
    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('description', null);
      this.set('newUserExperience', this.get('store').createRecord('user-wexperience'));
    },
    addNewExperience(userId) {
      this.set('didValidate', true);
      let user = this.get('store').peekRecord('user', userId);
      let userExperience = this.get('newUserExperience');
      userExperience.set('projectName', userExperience.get('projectName'));
      userExperience.set('client', userExperience.get('client'));
      userExperience.set('role', userExperience.get('role'));
      userExperience.set('startDate', userExperience.get('startDate'));
      userExperience.set('endDate', userExperience.get('endDate'));
      userExperience.set('description', this.get('description'));
      userExperience.save().then((userExperience) => {
        if (userExperience.get('validations.isValid')) {
          this.set('didValidate', false);
          user.get('userWexperiences').pushObject(userExperience);
          this.set('newUserExperience', this.get('store').createRecord('user-wexperience'));
          this.set('description', null);
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new work experience.', 'Success!');
        }
      });
    },
    selectChange(prop, val) {
      this.set(prop, val);
    },
    toggleWarningModal(id, name) {
      this.set('currentExpirance', this.get('store').peekRecord('user-wexperience', id));
      this.set('currentExpiranceName', name);
      this.toggleProperty('isShowingWarningModal');
    },
    deleteWorkExpirience() {
      this.get('currentExpirance').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted experience.', 'Success!');
      });
    }

  }
});
