import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  name(i) {
    return faker.list.cycle('JavaScript', 'Java', 'C#','XML', 'HTML5', 'CSS3', 'GIT', 'SVN')(i);
  },
  count() {
    return faker.random.number(9);
  },
  certificate() {
    return faker.random.number(9);
  }
});
