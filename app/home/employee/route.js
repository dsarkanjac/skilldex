import Ember from 'ember';
const {
  RSVP
} = Ember;

export default Ember.Route.extend({
  model: function(params) {
    return RSVP.hash({
      user: this.store.peekRecord('user', params.id),
      skills: this.store.findAll('skill'),
      educations: this.store.peekAll('education'),
      otherSkills: this.store.findAll('user-other')
    });
  },
  afterModel(model) {
    return model.user.get('userWexperiences');
  }
});
