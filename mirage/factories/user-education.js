import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  grade() {
    return faker.random.number(5);
  },
  startDate() {
    return faker.date.past();
  },
  endDate() {
    return faker.date.past();
  },
  note() {
    return faker.lorem.sentence();
  }
});
