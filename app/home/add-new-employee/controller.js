/* global toastr */

import Ember from 'ember';

export default Ember.Controller.extend({
  actions: {
    save() {
      this.get('model').save().then(() => {
        toastr.success('You have successfully added a new candidate.', 'Candidate added!');
        this.set('didValidate', false);
        Ember.run.next(() => {
          this.set('model', this.store.createRecord('user'));
        });
      });
    }
  }
});
