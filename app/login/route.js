import Ember from 'ember';
import UnauthenticatedRouteMixin from 'ember-simple-auth/mixins/unauthenticated-route-mixin';

const {
  inject: {
    service
  }
} = Ember;
export default Ember.Route.extend(UnauthenticatedRouteMixin, {
  session: service(),
  actions: {
    authenticate() {
      this.controller.set('signingIn', true);
      this.get('session').authenticate('authenticator:torii', 'google')
        .then(() => {
          this.transitionTo('home');
        })
        .catch((reason) => {
          this.set('errorMessage', reason.error || reason);
        });
    }
  }
});
