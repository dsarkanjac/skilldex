import Ember from 'ember';
import Base from 'ember-simple-auth/authenticators/base';

const {
  inject: {
    service
  },
  RSVP,
  isNone
} = Ember;

export default Base.extend({
  cookies: service(),
  session: service(),

  ajax: service(),
  restore() {
    let sessionIdExists = !isNone(this.get('cookies').read('JSESSIONID'));
    let csrfTokenExists = !isNone(this.get('cookies').read('XSRF-TOKEN'));
    let activeUserExists = !isNone(this.get('cookies').read('ACTIVE-USER'));

    if (sessionIdExists && csrfTokenExists && activeUserExists) {
      return RSVP.resolve();
    } else {
      return RSVP.reject();
    }

  },

  authenticate(data) {
    return new RSVP.Promise((resolve, reject) => {
      this.get('ajax').request('/token', data).then(response => {
        // this.get('ajax').request('http://office.tmns.rs:9000/skilldex/principal', data);
        Ember.Logger.debug('res:', response);
        Ember.run(() => {
          resolve(response);
        });
      }, xhr => {
        Ember.run(() => {
          reject(xhr.responseJSON || xhr.responseText);
        });
      });
    });
  },

  invalidate() {
    return RSVP.resolve();
  }

});
