export const EDUCATION_TYPE_LIST = [{
  id: 'high-school',
  name: 'High school'
}, {
  id: 'university',
  name: 'University'
}, {
  id: 'training',
  name: 'Training'
}, {
  id: 'certificate',
  name: 'Certificate'
}];
