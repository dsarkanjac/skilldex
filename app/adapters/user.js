import DS from 'ember-data';
import ENV from 'skilldex/config/environment';

export default DS.RESTAdapter.extend({
  namespace: (ENV.baseURL === '/') ? '' : 'skilldex'
  // host: 'http://office.tmns.rs:9000',
});
