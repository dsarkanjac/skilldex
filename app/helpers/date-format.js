/* globals moment */
import Ember from 'ember';

export function dateFormat(params) {
  let date = params[0];
  let format = params[1];
  if (typeof(format) !== 'string') {
    format = 'MMMM DD, YYYY';
  }

  if (date === null) {
    date = Date.now();
  }

  return moment(date).format(format);
}

export default Ember.Helper.helper(dateFormat);
