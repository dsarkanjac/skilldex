import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import ENV from 'skilldex/config/environment';

const {
  inject: {
    service
  }
} = Ember;
export default Ember.Route.extend(ApplicationRouteMixin, AuthenticatedRouteMixin, {
  ajax: service(),
  session: service(),
  cookies: service(),
  model: function() {
    return Ember.RSVP.hash({
      users: this.store.findAll('user'),
      skills: this.store.findAll('skill'),
      educations: this.store.findAll('education')
    });
  },
  afterModel(model) {
    return this.get('ajax').request(`${ENV.baseURL}principal`).then(res => {
      return this.get('session').set(
        'activeUser',
        model.users.find(user => {
          return +user.get('id') === +res.principal.id;
        }));
    });
  }
});
