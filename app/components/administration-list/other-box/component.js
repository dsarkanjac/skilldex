/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  didValidate: false, // Used for form validation.
  newOtherSkill: null,
  currentOtherSkill: null,

  otherSkillsList: Ember.computed('otherSkills.[]', 'filter', function() {
    if (!Ember.isNone(this.get('otherSkills'))) {
      return this.get('otherSkills').filter(skill => {
        let filterName = null;
        if (Ember.isPresent(this.filter)) {
          filterName = this.filter.toLowerCase();
        }
        if (!skill.get('validations.isValid')) {
          return;
        }

        return (skill.get('name').toLowerCase().includes(filterName) ||
          skill.get('value').toLowerCase().includes(filterName) || Ember.isEmpty(this.filter));
      });
    }
  }),

  isFiltering: Ember.computed('filter', function() {
    return !Ember.isBlank(this.get('filter'));
  }),

  init() {
    this._super(...arguments);
    this.set('newOtherSkill', this.get('store').createRecord('user-other'));
  },

  actions: {
    updateOtherSkillName(skillId, value) {
      let skill = this.get('store').peekRecord('user-other', skillId);
      skill.set('name', value);
      skill.save();
    },

    updateOtherSkillDescription(skillId, value) {
      let skill = this.get('store').peekRecord('user-other', skillId);
      skill.set('value', value);
      skill.save();
    },

    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('newOtherSkill', this.get('store').createRecord('user-other'));

    },

    addNewOtherSkill() {
      this.set('didValidate', true);
      let skill = this.get('newOtherSkill');
      if (Ember.isNone(skill.get('value'))) {
        skill.set('value', '');
      }else {
        skill.set('value', skill.get('value').trim());
      }
      skill.save().then(() => {
        if (skill.get('validations.isValid')) {
          this.set('didValidate', false);
          this.set('newOtherSkill', this.get('store').createRecord('user-other'));
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new skill.', 'Success!');
        }
      });
    },

    clearFilter() {
      this.set('filter', null);
    },

    toggleWarningModal(skillId) {
      this.set('currentOtherSkill', this.get('store').peekRecord('user-other', skillId));
      this.toggleProperty('isShowingWarningModal');
    },

    deleteSkill() {
      this.get('currentOtherSkill').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted skill.', 'Success!');
      });
    }
  }
});
