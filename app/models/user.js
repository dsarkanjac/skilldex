import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import Ember from 'ember';

import {
  hasMany
} from 'ember-data/relationships';
import {
  validator,
  buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
  firstName: validator('presence', true),
  lastName: validator('presence', true),

  username: [
    validator('presence', true),
    validator('format', {
      type: 'email'
    })
  ]

});
export default Model.extend(Validations, {
  firstName: attr('string'),
  lastName: attr('string'),
  maidenName: attr('string'),
  username: attr('string'),
  birthday: attr('date'),
  address: attr('string'),
  phone: attr('string'),
  bio: attr('string'),
  status: attr('string'),
  imageUrl: attr('string'),
  isCandidate: Ember.computed('userRoles.@each.name', function() {
    return this.get('userRoles.content').length < 1;
  }),
  yearsOfExperience: Ember.computed('userWexperiences', function() {
    let allExperience = 0;
    this.get('userWexperiences').forEach((workExperience) => {
      let years = this.countYears(workExperience.get('startDate'), workExperience.get('endDate'));
      allExperience += years;
    });
    return allExperience;
  }),
  countYears: function(startDate, endDate) {
    if (Ember.isNone(endDate)) {
      let today = new Date();
      var age = today.getFullYear() - startDate.getFullYear();
      let month = today.getMonth() - startDate.getMonth();
      if (month < 0 || (month === 0 && today.getDate() < startDate.getDate())) {
        age--;
      }
      return age;
    } else {
      var newAge = endDate.getFullYear() - startDate.getFullYear();
      let month = endDate.getMonth() - startDate.getMonth();
      if (month < 0 || (month === 0 && endDate.getDate() < startDate.getDate())) {
        newAge--;
      }
      return newAge;
    }
  },
  userSkills: hasMany('user-skill'),
  userEducations: hasMany('user-education'),
  userRoles: hasMany('user-role'),
  userWexperiences: hasMany('user-wexperience'),
  userOther: hasMany('user-other')

});
