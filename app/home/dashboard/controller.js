import Ember from 'ember';

export default Ember.Controller.extend({
  ajax: Ember.inject.service(),
  averageAge: Ember.computed('model.users', function() {
    let average = null;
    let yearsList = [];
    this.get('model.users').forEach(user => {
      if (!Ember.isNone(user.get('birthday'))) {
        let today = new Date();
        let birthDate = new Date(user.get('birthday'));
        var age = today.getFullYear() - birthDate.getFullYear();
        let month = today.getMonth() - birthDate.getMonth();
        if (month < 0 || (month === 0 && today.getDate() < birthDate.getDate())) {
          age--;
        }
        yearsList.push(age);
      }
    });
    let total = 0;
    for (var i = 0; i < yearsList.length; i++) {
      total += yearsList[i];
    }
    average = total / yearsList.length;
    return average.toFixed(0);
  })
});
