import Ember from 'ember';

const {
  RSVP,
} = Ember;

export default Ember.Route.extend({
  model() {
    return RSVP.hash({
      skills: this.store.findAll('skill'),
      educations: this.store.findAll('education'),
      otherSkills: this.store.findAll('user-other'),
    });
  },
});
