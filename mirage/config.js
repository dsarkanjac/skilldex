export default function() {

  // These comments are here to help you get started. Feel free to delete them.

  /*
    Config (with defaults).

    Note: these only affect routes defined *after* them!
  */

  // this.urlPrefix = '';    // make this `http://localhost:8080`, for example, if your API is on a different server
  // this.namespace = '';    // make this `api`, for example, if your API is namespaced
  // this.timing = 400;      // delay for each request, automatically set to 0 during testing

  /*
    Shorthand cheatsheet:

    this.get('/posts');
    this.post('/posts');
    this.get('/posts/:id');
    this.put('/posts/:id'); // or this.patch
    this.del('/posts/:id');
  */
  this.passthrough('http://office.tmns.rs:9000/**');

  // Users.
  this.get('/users');

  this.get('/users/:id/we', ({
    userWexperience
  }, request) => {
    return userWexperience.where({
      userId: request.params.id
    });
  });

  this.get('/users/:id/other', ({
    userOther
  }, request) => {
    return userOther.where({
      userId: request.params.id
    });
  });
  this.get('/users/:id/roles', ({
    userRole
  }, request) => {
    return userRole.where({
      userId: request.params.id
    });
  });
  this.get('/users/:id/education', ({
    userEducation
  }, request) => {
    return userEducation.where({
      userId: request.params.id
    });
  });
  this.get('/users/:id/skill', ({
    userSkill
  }, request) => {
    return userSkill.where({
      userId: request.params.id
    });
  });
  this.post('/users');
  this.put('/users/:id');

  // Dashboard.
  this.get('/dashboard/assigned', () => {
    return {
      assigned: 5,
      unassigned: 13,
    };
  });

  this.get('/dashboard/topskills', ({
    topSkill
  }) => {
    return topSkill.all();
  });

  // Skills.
  this.get('/skills');
  this.put('/skills/:id');
  this.post('/skills');
  this.delete('/skills/:id');

  // Educations.
  this.get('/educations');
  this.put('/educations/:id');
  this.post('/educations');
  this.delete('/educations/:id');

  // User skills.
  this.post('/userSkills', ({
    userSkill,
    skill
  }, request) => {
    let json = JSON.parse(request.requestBody);
    let newRecord = userSkill.create({
      experience: json.userSkill.experience,
      grade: json.userSkill.grade
    });
    newRecord.skill = skill.find(json.userSkill.skill.id);
    return newRecord.save();
  });
  this.delete('/userSkills/:id');

  // User other.
  this.get('/userOthers');
  this.put('/userOthers/:id');
  this.post('/userOthers');
  this.delete('/userOthers/:id');

  // User educations.
  this.post('/userEducations', ({
    userEducation,
    education
  }, request) => {
    let json = JSON.parse(request.requestBody);
    let newRecord = userEducation.create({
      grade: json.userEducation.grade,
      startDate: json.userEducation.startDate,
      endDate: json.userEducation.endDate,
      note: json.userEducation.note
    });
    newRecord.education = education.find(json.userEducation.education.id);
    return newRecord.save();
  });
  this.delete('/userEducations/:id');

  // User work experience.
  this.post('/userWexperiences');
  this.delete('/userWexperiences/:id');

  // Authentication.
  this.get('/token', () => {
    document.cookie = `JSESSIONID=AB83D1F7C5FDE69BCE8F9D82D4FA5A9A;  domain=localhost; path=/;`;
    document.cookie = `XSRF-TOKEN=b2c6dacb-6d53-43ba-8adb-3854e33b8fa2;  domain=localhost; path=/;`;
    document.cookie = `ACTIVE-USER=1; domain=localhost; path=/;`;

    return;
  });

  this.get('/principal', () => {
    return {
      'principal': {
        'id': 1,
        'username': 'istajic@tmns.com',
        'imageUrl': 'https://lh5.googleusercontent.com/-oSwVMAAgHWA/AAAAAAAAAAI/AAAAAAAAABQ/BtMnKM8EjSc/photo.jpg'
      },
    };
  });
}
