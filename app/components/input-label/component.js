import Ember from 'ember';

/**
 * @public
 *
 * @param value - value to be shown/edited.
 * @param focus-out - action to preform on focus-out
 */
export default Ember.Component.extend({
  isEditingMode: false,
  isNullable: true,
  type: 'text',
  placeholderText: 'None',
  hidePlaceholder: false,
  validationRegex: null,
  isRegexValid: false,
  customValidationMessage: null,
  checkDuplicateList: null,
  checkDuplicateProperty: '',
  validationMessage: Ember.computed('rawValue', function() {
    if (Ember.isBlank(this.get('rawValue').trim())) {
      return 'Value can\'t be blank.';
    }

    return this.get('customValidationMessage') || `Value '${this.get('rawValue')}' is reserved.`;
  }),

  showValidationError: false,
  hideValdiation() {
    this.set('showValidationError', false);
  },

  showValdiation() {
    this.set('showValidationError', true);
  },

  isValid: Ember.computed('rawValue', function() {
    if (this.get('checkDuplicateList') && (this.get('rawValue') !== this.get('value'))) {
      if (this.get('checkDuplicateList').filterBy(this.get('checkDuplicateProperty'), this.get('rawValue')).length > 0) {
        this.set('validationMessage', this.get('checkDuplicateMessage'));
        return false;
      }
    }

    if (this.get('validationRegex')) {
      const rule = new RegExp(this.get('validationRegex'));
      if (rule.test(this.get('rawValue'))) {
        return this.get('isRegexValid');
      } else {
        return !this.get('isRegexValid');
      }
    }

    if (!this.get('isNullable') && Ember.isBlank(this.get('rawValue').trim())) {
      return false;
    }

    return true;
  }),

  actions: {
    toggleEditMode: function(value) {
      this.toggleProperty('isEditingMode');
      if (!this.get('isValid')) {
        this.set('rawValue', this.get('value'));
        return;
      }

      if (this.attrs['focus-out'] && !this.get('isEditingMode')) {
        this.attrs['focus-out'](value.trim());
      }
    },

    onKeyDown(value, e) {
      const {
        keyCode,
      } = e;
      switch (keyCode) {
        case 13:
          if (!this.get('isValid')) {
            this.showValdiation();
            Ember.run.debounce(this, this.hideValdiation, 2000, false);
            e.preventDefault();
            return;
          }

          this.send('toggleEditMode', value.trim());
          break;
        case 27:
          this.set('rawValue', this.get('value'));
          this.toggleProperty('isEditingMode');
      }
    },
  },
  didReceiveAttrs() {
    this.set('rawValue', this.get('value'));
  },

  didRender() {
    if (this.get('isEditingMode')) {
      this.$('input').focus();
    }
  },
});
