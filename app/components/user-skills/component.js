/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  newUserSkill: null,
  selectedSkill: null,
  didValidate: false,
  init() {
    this._super(...arguments);
    this.set('newUserSkill', this.get('store').createRecord('user-skill'));
  },
  actions: {
    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('newUserSkill', this.get('store').createRecord('user-skill'));
      this.set('selectedSkill', null);
    },
    addNewSkill(userId) {
      this.set('didValidate', true);
      let user = this.get('store').peekRecord('user', userId);
      let userSkill = this.get('newUserSkill');
      userSkill.set('grade', userSkill.get('grade'));
      userSkill.set('experience', userSkill.get('experience'));
      userSkill.set('skill', this.get('selectedSkill'));
      userSkill.save().then((userSkill) => {
        if (userSkill.get('validations.isValid')) {
          this.set('didValidate', false);
          user.get('userSkills').pushObject(userSkill);
          this.set('newUserSkill', this.get('store').createRecord('user-skill'));
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new skill.', 'Success!');
        }
      });
    },
    selectChange(prop, val) {
      this.set(prop, val);
    },
    toggleWarningModal(id, name) {
      this.set('currentSkill', this.get('store').peekRecord('user-skill', id));
      this.set('currentSkillName', name);
      this.toggleProperty('isShowingWarningModal');
    },
    deleteSkill() {
      this.get('currentSkill').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted skill.', 'Success!');
      });
    }
  }
});
