/*jshint node:true*/
/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    sassOptions: {
      includePaths: ['app'],
    },
  });

  const Funnel = require('broccoli-funnel');
  const BroccoliMergeTrees = require('broccoli-merge-trees');

  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  const fontAwesome = new Funnel('bower_components/font-awesome/fonts', {
    destDir: '/fonts',
  });

  const images = new Funnel('bower_components/iCheck/skins/square', {
    destDir: '/assets',
  });
  const bootstrapFonts = new Funnel('bower_components/bootstrap/fonts', {
    destDir: '/fonts'
  });

  const customImages = new Funnel('vendor/images', {
    destDir: '/img'
  });

  app.import('vendor/inspinia/js/inspinia.js');
  app.import('vendor/inspinia/js/bootstrap.min.js');
  app.import('vendor/inspinia/js/jquery.slimscroll.min.js');
  app.import('vendor/morris.js-0.5.1/morris.min.js');
  app.import('bower_components/iCheck/icheck.js');
  app.import('bower_components/lodash/lodash.js');
  app.import('bower_components/raphael/raphael.min.js');
  app.import('vendor/peity-chart/jquery.peity.min.js');
  app.import('bower_components/bootstrap/js/tab.js');
  app.import('bower_components/toastr/toastr.min.js');
  app.import('bower_components/Chart.js/dist/Chart.min.js');
  // app.import('bower_components/Chart.js/src/core/core.js');

  app.import('bower_components/toastr/toastr.min.css');
  app.import('vendor/inspinia/css/bootstrap.min.css');
  app.import('vendor/inspinia/css/normalize.css');
  app.import('vendor/inspinia/css/style.min.css');
  app.import('vendor/inspinia/css/animate.css');
  app.import('bower_components/iCheck/skins/square/purple.css');
  app.import('bower_components/font-awesome/css/font-awesome.css');
  app.import('vendor/morris.js-0.5.1/morris.css');

  const outputTree = new BroccoliMergeTrees([
    fontAwesome,
    images,
    customImages,
    bootstrapFonts,
    app.toTree(),
  ]);

  return outputTree;
};
