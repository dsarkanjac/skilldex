import Model from 'ember-data/model';
import attr from 'ember-data/attr';

import {
  belongsTo
} from 'ember-data/relationships';

import {
  validator,
  buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
  startDate: validator('presence', true)
});

export default Model.extend(Validations, {
  grade: attr('string'),
  startDate: attr('date'),
  endDate: attr('date'),
  note: attr('string'),

  education: belongsTo('education')
});
