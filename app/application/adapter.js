import RESTAdapter from 'ember-data/adapters/rest';
import ENV from 'skilldex/config/environment';

import Ember from 'ember';
const {
  Inflector: {
    inflector
  }
} = Ember;

export default RESTAdapter.extend({
  namespace: (ENV.baseURL === '/') ? '' : 'skilldex'
  // updateRecord: function(store, type, snapshot) {
  //
  //   var data = this.serialize(snapshot, {
  //     includeId: true
  //   });
  //   let url = `/${inflector.pluralize(type.modelName)}`;
  //   return new Ember.RSVP.Promise(function(resolve, reject) {
  //     Ember.$.ajax({
  //       type: 'POST',
  //       url: url,
  //       dataType: 'json',
  //       data: JSON.stringify(data)
  //     }).then(function(data) {
  //       Ember.run(null, resolve, data);
  //     }, function(jqXHR) {
  //       jqXHR.then = null; // tame jQuery's ill mannered promises
  //       Ember.run(null, reject, jqXHR);
  //     });
  //   });
  // }
});
