import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return faker.lorem.sentence();
  },
  value() {
    return faker.lorem.paragraph();
  }
});
