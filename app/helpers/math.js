import Ember from 'ember';

export function math(params) {
  params[0] = parseFloat(params[0]);
  params[2] = parseFloat(params[2]);
  return {
    '+': params[0] + params[2],
    '-': params[0] - params[2],
    '*': params[0] * params[2],
    '/': params[0] / params[2],
    '%': params[0] % params[2],
  }[params[1]];
}

export default Ember.Helper.helper(math);
