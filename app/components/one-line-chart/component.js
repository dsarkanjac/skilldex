/*global Morris*/
import Ember from 'ember';
import ENV from 'skilldex/config/environment';

export default Ember.Component.extend({
  ajax: Ember.inject.service(),
  didInsertElement() {
    this.get('ajax').request(`${ENV.baseURL}dashboard/topskills`).then(data => {
      Morris.Line({
        element: 'morris-one-line-chart',
        data: data.topSkills.map(record => {
          return {
            'name': record.name,
            'count': parseInt(record.count)
          };
        }),
        xkey: 'name',
        ykeys: ['count'],
        resize: true,
        lineWidth: 4,
        labels: ['Employees'],
        lineColors: ['#5C5094'],
        pointSize: 5,
        parseTime: false
      });
    });
  }
});
