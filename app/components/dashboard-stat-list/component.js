import Ember from 'ember';
import ENV from 'skilldex/config/environment';

export default Ember.Component.extend({
  ajax: Ember.inject.service(),
  didInsertElement() {
    this.get('ajax').request(`${ENV.baseURL}dashboard/assigned`).then(data => {
      let bla = document.getElementById('stat-list');
      new Chart(bla, {
        type: 'doughnut',
        height: 100,
        options: {
          legend: {
            display: true,
            labels: {
              boxWidth: 10
            }
          }
        },
        data: {
          labels: ["Assigned", "Unassigned"],
          datasets: [{
            data: [data.assigned, data.unassigned],
            backgroundColor: ['rgba(92, 80, 148, 0.55)', 'rgba(61, 52, 103, 0.75)'],
            borderColor: ['rgba(92, 80, 148, 0.55)', 'rgba(61, 52, 103, 0.75)'],
            borderWidth: [0, 0]
          }]
        }
      });
    });
  }
});
