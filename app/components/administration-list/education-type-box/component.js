/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  didValidate: false, // Used for form validation.
  newEducation: null,
  currentEducation: null,

  educationsList: Ember.computed('educations.[]', 'filter', function() {
    if (!Ember.isNone(this.get('educations'))) {
      return this.get('educations').filter(education => {
        let filterName = null;
        if (Ember.isPresent(this.filter)) {
          filterName = this.filter.toLowerCase();
        }
        if (!education.get('validations.isValid')) {
          return;
        }
        return education.get('type') === this.get('title') &&
          (education.get('name').toLowerCase().includes(filterName) || Ember.isEmpty(this.filter));
      });
    }
  }),

  isFiltering: Ember.computed('filter', function() {
    return !Ember.isBlank(this.get('filter'));
  }),

  init() {
    this._super(...arguments);
    this.set('newEducation', this.get('store').createRecord('education'));
  },
  actions: {
    updateEducationName(educationId, value) {
      let education = this.get('store').peekRecord('education', educationId);
      education.set('name', value);
      education.save();
    },

    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('newEducation', this.get('store').createRecord('education'));
    },

    addNewEducation() {
      this.set('didValidate', true);
      let education = this.get('newEducation');
      education.set('type', this.get('title'));
      education.save().then(() => {
        if (education.get('validations.isValid')) {
          this.set('didValidate', false);
          this.set('newEducation', this.get('store').createRecord('education'));
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new education.', 'Success!');
        }
      });
    },

    clearFilter() {
      this.set('filter', null);
    },

    toggleWarningModal(educationId) {
      this.set('currentEducation', this.get('store').peekRecord('education', educationId));
      this.toggleProperty('isShowingWarningModal');
    },

    deleteEducation() {
      this.get('currentEducation').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted education.', 'Success!');
      });
    }
  },
});
