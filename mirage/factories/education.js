import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return faker.company.companyName();
  },
  type() {
    return faker.random.arrayElement(['High school', 'University', 'Training', 'Certificate']);
  }
});
