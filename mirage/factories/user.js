import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  firstName() {
    return faker.name.firstName();
  },
  maidenName() {
    return faker.name.lastName();
  },
  lastName() {
    return faker.name.lastName();
  },
  birthday() {
    return faker.date.between('1950', '2000');
  },
  username() {
    return faker.internet.email();
  },
  address() {
    return `${faker.address.streetAddress()}, ${faker.address.city()}` ;
  },
  phone() {
    return faker.phone.phoneNumber();
  },
  bio() {
    return faker.lorem.paragraph();
  },
  status() {
    return faker.random.number(2);
  },
  imageUrl() {
    return faker.image.avatar();
  },
});
