import DS from 'ember-data';
import ENV from 'skilldex/config/environment';

export default DS.RESTSerializer.extend({
  // attrs: {
  //   userSkills: {
  //     embedded: 'always'
  //   },
  //   userEducations: {
  //     embedded: 'always'
  //   },
  //   userRoles: {
  //     embedded: 'always'
  //   }
  // },
  serialize: function(snapshot) {
    var json = {};

    snapshot.eachAttribute(function(name) {
      json[name] = snapshot.attr(name);
    });

    return json;
  },
  normalize() {
    let json = this._super(...arguments);
    json.data.relationships.userWexperiences = {
      'links': {
        'related': `${ENV.baseURL}users/${json.data.id}/we`
      }
    };
    json.data.relationships.userOther = {
      'links': {
        'related': `${ENV.baseURL}users/${json.data.id}/other`
      }
    };
    json.data.relationships.userSkills = {
      'links': {
        'related': `${ENV.baseURL}users/${json.data.id}/skill`
      }
    };
    json.data.relationships.userRoles = {
      'links': {
        'related': `${ENV.baseURL}users/${json.data.id}/roles`
      }
    };
    json.data.relationships.userEducations = {
      'links': {
        'related': `${ENV.baseURL}users/${json.data.id}/education`
      }
    };

    return json;
  }
});
