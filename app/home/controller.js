/* global toastr */

import Ember from 'ember';

toastr.options = {
  'closeButton': true,
  'debug': false,
  'progressBar': true,
  'onclick': null,
  'showDuration': '150',
  'hideDuration': '1000',
  'preventDuplicates': true,
  'timeOut': '3000',
  'extendedTimeOut': '1000',
  'showEasing': 'swing',
  'hideEasing': 'linear',
  'showMethod': 'show',
  'hideMethod': 'hide'
};

export default Ember.Controller.extend({
  session: Ember.inject.service('session'),
  ajax: Ember.inject.service('ajax'),
  actions: {
    invalidateSession() {
      this.get('session').invalidate();
    },
    authenticate() {
      this.get('session').authenticate('authenticator:cookie-auth').catch(
        err => {
          Ember.Logger.error(err);
        }
      );
    }
  }
});
