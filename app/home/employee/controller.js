import Ember from 'ember';

export default Ember.Controller.extend({
  isShowingModal: false,
  certificateNumber: Ember.computed('model.user.userEducations', function() {
    let certificateCount = 0;
    this.get('model.user.userEducations').forEach((userEducation) => {
      if (userEducation.get('education.type') === 'Certificate') {
        certificateCount++;
      }
    });
    return certificateCount;
  }),
  actions: {
    toggleEditModal() {
      this.toggleProperty('isShowingModal');
    },
    cancelEditModal() {
      console.log('close');
      this.get('model.user').rollbackAttributes();
      this.toggleProperty('isShowingModal');
    },

    updateUser() {
      let user = this.get('model.user');
      if (user.get('validations.isValid')) {
        user.save();
        this.toggleProperty('isShowingModal');
      }
    }
  }
});
