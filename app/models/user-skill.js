import Model from 'ember-data/model';
import attr from 'ember-data/attr';

import {
  belongsTo
} from 'ember-data/relationships';

import {
  validator,
  buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
  grade: validator('presence', true),
  experience: validator('presence', true)
});

export default Model.extend(Validations, {
  grade: attr('string'),
  experience: attr('string'),
  order: attr('string'),
  topSkill: attr('string'),
  obsolete: attr('string'),
  skill: belongsTo('skill')
});
