import {
  Model,
  hasMany
} from 'ember-cli-mirage';

export default Model.extend({
  userSkills: hasMany(),
  userEducations: hasMany(),
  userWexperiences: hasMany(),
  userOther: hasMany(),
  userRoles: hasMany()


});
