import Ember from 'ember';
const {
  inject: {
    service
  }
} = Ember;

export default Ember.Route.extend({
  session: service(),
  actions: {
    didTransition() {
      if (+this.get('session.activeUser.id') === 1) {
        this.transitionTo('home.dashboard');
      } else {
        this.transitionTo('home.employees');
      }
    }
  }
});
