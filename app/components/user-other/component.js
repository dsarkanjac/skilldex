/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  newUserOther: null,
  didValidate: false,
  init() {
    this._super(...arguments);
    this.set('newUserOther', this.get('store').createRecord('user-other'));
  },
  actions: {
    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('value', null);
      this.set('newUserOther', this.get('store').createRecord('user-other'));
    },
    addNewOther(userId) {
      this.set('didValidate', true);
      let user = this.get('store').peekRecord('user', userId);
      let userOther = this.get('newUserOther');
      userOther.set('name', userOther.get('name'));
      userOther.set('value', this.get('value'));
      userOther.save().then((userOther) => {
        if (userOther.get('validations.isValid')) {
          this.set('didValidate', false);
          user.get('userOther').pushObject(userOther);
          this.set('newUserOther', this.get('store').createRecord('user-other'));
          this.set('value', null);
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added other skill.', 'Success!');
        }
      });
    },
    selectChange(prop, val) {
      this.set(prop, val);
    },
    toggleWarningModal(id, name) {
      this.set('currentOther', this.get('store').peekRecord('user-other', id));
      this.set('currentOtherName', name);
      this.toggleProperty('isShowingWarningModal');
    },
    deleteUserOther() {
      this.get('currentOther').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted other skill.', 'Success!');
      });
    }
  }
});
