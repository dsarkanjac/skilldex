import Ember from 'ember';

export default Ember.Component.extend({
  didValidate: false, // Used for form validation.

  actions: {
    save() {
      this.set('didValidate', true);
      if (this.get('user.validations.isValid')) {
        this.attrs.saveUser();
        this.set('phone', null);
        this.set('biography', null);
      }
    }
  }

});
