/* global $*/
import Ember from 'ember';

export default Ember.Component.extend({
  didInsertElement() {
    $(".line").peity("line", {
      fill: '#5C5094',
      stroke: '#169c81',
    });
    $(".bar").peity("bar", {
      fill: ["#5C5094", "#d7d7d7"]
    });
  }
});
