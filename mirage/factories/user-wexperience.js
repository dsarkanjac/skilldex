import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  projectName() {
    return faker.company.catchPhrase();
  },
  startDate() {
    return faker.date.past();
  },
  role() {
    return faker.name.jobTitle();
  },
  endDate() {
    return faker.date.past();
  },
  client() {
    return faker.company.companyName();
  },
  description() {
    return faker.lorem.sentences();
  },
});
