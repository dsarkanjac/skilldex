import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  emailList: Ember.computed('filteredUsers', function() {
    return this.get('filteredUsers').mapBy('username');
  }),
  firstNameList: Ember.computed('filteredUsers', 'users', function() {
    return this.get('filteredUsers').mapBy('firstName').uniq();
  }),
  lastNameList: Ember.computed('filteredUsers', 'users', function() {
    return this.get('filteredUsers').mapBy('lastName').uniq();
  }),
  currentUser: null,
  selectedUsername: null,
  selectedLastName: null,
  selectedFirstName: null,
  employeeSelected: false,
  managerSelected: false,
  selectedRolesObs: Ember.observer('employeeSelected', 'managerSelected', function() {
    let arr = this.get('selectedRoles');
    arr.addObject(Ember.Object.create({
      type: this.get('employeeSelected') ? 'Employee' : ''
    }));
    arr.addObject(Ember.Object.create({
      type: this.get('managerSelected') ? 'Manager' : ''
    }));
  }),
  selectedRoles: [],
  educationCount: [{
    id: 1
  }],
  skillCount: [{
    id: 1
  }],
  selectedEducationList: [],
  selectedSkillList: [],
  filteredUsers: Ember.computed('employeeSelected', 'managerSelected', 'selectedRoles.@each.type', 'selectedEducationList.@each.name', 'selectedSkillList.@each.name', 'selectedSkillList.@each.grade', 'selectedSkillList.@each.experience', 'selectedUsername', 'selectedFirstName', 'selectedLastName', function() {
    let employee = this.get('employeeSelected') ? 'Employee' : '';
    let manager = this.get('managerSelected') ? 'Manager' : '';

    let roles = [];
    if (employee || manager) {
      roles.addObject(employee);
      roles.addObject(manager);
    }

    return this.get('users').filter(user => {
      let eduList = this.get('selectedEducationList');
      let skillList = this.get('selectedSkillList');
      let skillVal = true;
      if (!Ember.isEmpty(eduList)) {
        eduList = eduList.filter(edu => {
          if (!Ember.isEmpty(edu.get('name'))) {
            return edu;
          }
        });
      }

      if (!Ember.isEmpty(skillList)) {
        skillList.filter(skill => {
          let grade = parseInt(Ember.isEmpty(skill.get('grade')) ? 0 : skill.get('grade'));
          let experience = parseInt(Ember.isEmpty(skill.get('experience')) ? 0 : skill.get('experience'));
          let skillNames = user.get('userSkills').mapBy('skill.name');
          if (!skillNames.contains(skill.get('name'))) {
            skillVal = false;
          } else {
            let userGrade = parseInt(user.get('userSkills').findBy('skill.name', skill.get('name')).get('grade'));
            let userExperience = parseInt(user.get('userSkills').findBy('skill.name', skill.get('name')).get('experience'));
            if (!(grade <= userGrade && experience <= userExperience)) {
              skillVal = false;
            }

          }
        });
      }
      return (
        ((_.intersection(user.get('userRoles').mapBy('role.name'), roles).length > 0) || Ember.isEmpty(roles)) &&
        ((user.get('username') === this.get('selectedUsername')) || Ember.isEmpty(this.get('selectedUsername'))) &&
        (user.get('firstName').includes(this.get('selectedFirstName')) ||
          Ember.isEmpty(this.get('selectedFirstName'))) &&
        (user.get('lastName').includes(this.get('selectedLastName')) || Ember.isEmpty(this.get('selectedLastName'))) &&
        (_.intersection(eduList.mapBy('name'), user.get('userEducations').mapBy('education.name')).length > 0 ||
          Ember.isEmpty(eduList)) && (skillVal || Ember.isEmpty(skillList))
      );
    });
  }),
  didInsertElement() {
    let self = this;

    $('.i-checks input').iCheck({
      checkboxClass: 'icheckbox_square-purple',
      radioClass: 'iradio_square-purple'
    });

    $('.i-checks.emp input').on('ifClicked', function() {
      self.set('employeeSelected', !self.get('employeeSelected'));
      $(this).change();
    });

    $('.i-checks.man input').on('ifClicked', function() {
      self.set('managerSelected', !self.get('managerSelected'));
      $(this).change();
    });

    if ($(window).width() < 769) {
      $('.clients-list').on('click', '#employees table tr', function() {
        setTimeout(function() {
          $('html,body').animate({
            scrollTop: $('.user-details-col').offset().top
          });
        }, 200)
      })
    }
  },
  didRender() {
    if ($(window).width() > 769) {
      $('.edu-filter').css('min-height', 'auto');
      if ($('.edu-filter').height() < $('.edu-filter').parent('div').height()) {
        $('.edu-filter').css('min-height', $('.edu-filter').parent('div').height());
      }
    }
  },
  init() {
    this._super(...arguments);
    this.setProperties({
      selectedEducationList: [],
      selectedSkillList: [],
    });
  },
  actions: {
    userListDetail(id) {
      this.set('currentUser', this.get('store').peekRecord('user', id));
    },
    closeEmployeeDetail() {
      this.set('currentUser', null);
    },
    selectChange(prop, val) {
      this.set(prop, val);
    },
    addEducationRow() {
      this.get('educationCount').pushObject({
        id: this.get('educationCount').get('lastObject.id') + 1
      });
    },
    addSkillRow() {
      this.get('skillCount').pushObject({
        id: this.get('skillCount').get('lastObject.id') + 1
      });
    },
    removeEduRow(id) {
      let list = this.get('selectedEducationList');
      list.removeObject(list.findBy('id', id));

      $(`#edu_${id}`).remove();
      this.rerender();
    },
    removeSkillRow(id) {
      let list = this.get('selectedSkillList');
      list.removeObject(list.findBy('id', id));

      $(`#skill_${id}`).remove();
      this.rerender();
    },
    resetProp(prop) {
      this.set(prop, null);
    }
  }
});
