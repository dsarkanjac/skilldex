import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
});

Router.map(function() {
  this.route('home', {
      path: '/',
    }, function() {
    this.route('employees');
    this.route('dashboard');
    this.route('administration');
    this.route('add-new-employee');
    this.route('employee', {
      path: 'employee/:id'
    });
  });
  this.route('login');

});

export default Router;
