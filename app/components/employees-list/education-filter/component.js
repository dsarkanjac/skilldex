import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  selectedEducationtype: null,
  selectedEducationName: null,
  selectedEducationNameObs: Ember.observer('selectedEducationName', function() {
    if (this.get('selectedEducationList').findBy('id', this.get('rowId'))) {
      let obj = this.get('selectedEducationList').findBy('id', this.get('rowId'));
      obj.set('name', this.get('selectedEducationName'));
    } else {
      let obj = Ember.Object.create({
        id: this.get('rowId'),
        name: this.get('selectedEducationName')
      });

      this.get('selectedEducationList').addObject(obj);
    }
  }),
  eduType: Ember.computed('userEducations.[]', 'filteredUsers', 'selectedUsername', 'selectedLastName', 'selectedFirstName', function() {
    let eduTypes = [];
    if (this.get('selectedUsername') || this.get('selectedLastName') || this.get('selectedFirstName')) {
      this.get('filteredUsers').filter(user => {
        eduTypes.pushObject(user.get('userEducations').mapBy('education.type'));
      });
      return _.flatten(eduTypes).uniq();
    } else {
      return this.get('userEducations').mapBy('type').uniq();
    }
  }),
  eduName: Ember.computed('selectedEducationtype', function() {
    let eduArr = [];
    this.set('selectedEducationName', null);
    if (this.get('selectedEducationtype')) {
      eduArr = this.get('userEducations').filter((item) => {
        if (item.get('type') === this.get('selectedEducationtype')) {
          return item;
        }
      });
    }
    return eduArr.mapBy('name');
  }),
  actions: {
    selectChange(prop, val) {
      this.set(prop, val);
    }
  }
});
