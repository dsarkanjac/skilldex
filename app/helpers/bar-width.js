import Ember from 'ember';

export function barWidth(params) {
  let [width] = params;

  return Ember.String.htmlSafe(`width: ${parseInt(width)}0%;`);
}

export default Ember.Helper.helper(barWidth);
