import {
  Factory,
  faker
} from 'ember-cli-mirage';

export default Factory.extend({
  experience() {
    return faker.random.number(5);
  },
  grade() {
    return faker.random.number({'min': 1, 'max':10});
  },
  obsolete() {
    return faker.random.number(1);
  },
  order() {
    return faker.random.number(1);
  },
  topSkill() {
    return faker.random.number(1);
  }
});
