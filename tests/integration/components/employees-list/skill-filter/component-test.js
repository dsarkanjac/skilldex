import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('employees-list/skill-filter', 'Integration | Component | employees list/skill filter', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{employees-list/skill-filter}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#employees-list/skill-filter}}
      template block text
    {{/employees-list/skill-filter}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
