import Ember from 'ember';
const {
  String: {
    w
  }, isNone
} = Ember;
export default Ember.Service.extend({
  read(cookieName) {
    let rawCookieData = document.cookie;
    let cookies = w(rawCookieData).map(cookie => {
      let [name, value] = cookie.split('=');
      return {
        name,
        value
      };
    });
    if (!isNone(name)) {
      return cookies.find(cookie => {
        if (cookie.name === cookieName) {
          return cookie;
        } else {
          return false;
        }
      }).value;
    } else {
      return cookies;
    }
  }
});
