import Ember from 'ember';

const {
  RSVP
} = Ember;

export default Ember.Route.extend({
  model() {
    return RSVP.hash({
      users: this.store.peekAll('user'),
      userEducations: this.store.peekAll('education'),
      userSkills: this.store.findAll('skill')
    });
  }
});
