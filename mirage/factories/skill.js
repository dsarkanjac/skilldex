import {
  Factory, faker
} from 'ember-cli-mirage';

export default Factory.extend({
  name() {
    return faker.random.arrayElement(['JavaScript', 'Java', 'C#', 'XML', 'HTML5', 'CSS3', 'Python', 'SVN', 'Git']);
  },

  description() {
    return faker.lorem.sentence();
  },

  obsolete() {
    return faker.random.number(1);
  },

  type() {
    return faker.random.arrayElement(['Programing & modeling', 'Version control systems', 'Tibco', 'Oracle', 'Database', 'Opensource', 'Aurea', 'Language', 'Roles']);
  },
});
