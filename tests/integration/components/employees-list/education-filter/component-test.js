import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('employees-list/education-filter', 'Integration | Component | employees list/education filter', {
  integration: true
});

test('it renders', function(assert) {
  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{employees-list/education-filter}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#employees-list/education-filter}}
      template block text
    {{/employees-list/education-filter}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
