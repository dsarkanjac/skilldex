/* global toastr */
import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  isShowingWarningModal: false,
  didValidate: false, // Used for form validation.
  newSkill: null,
  currentSkill: null,

  skillsList: Ember.computed('skills.[]', 'filter', function() {
    if (!Ember.isNone(this.get('skills'))) {
      return this.get('skills').filter(skill => {
        let filterName = null;
        if (Ember.isPresent(this.filter)) {
          filterName = this.filter.toLowerCase();
        }
        if (!skill.get('validations.isValid')) {
          return;
        }
        return skill.get('type') === this.get('title') && (skill.get('name').toLowerCase().includes(filterName) ||
          skill.get('description').toLowerCase().includes(filterName) || Ember.isEmpty(this.filter));
      });
    }
  }),
  isFiltering: Ember.computed('filter', function() {
    return !Ember.isBlank(this.get('filter'));
  }),
  init() {
    this._super(...arguments);
    this.set('newSkill', this.get('store').createRecord('skill'));
  },
  actions: {
    updateSkillName(skillId, value) {
      let skill = this.get('store').peekRecord('skill', skillId);
      skill.set('name', value);
      skill.save();
    },

    updateSkillDescription(skillId, value) {
      let skill = this.get('store').peekRecord('skill', skillId);
      skill.set('description', value);
      skill.save();
    },

    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('newSkill', this.get('store').createRecord('skill'));

    },

    addNewSkill() {
      this.set('didValidate', true);
      let skill = this.get('newSkill');
      skill.set('type', this.get('title'));
      if (Ember.isNone(skill.get('description'))) {
        skill.set('description', '');
      }else {
        skill.set('description', skill.get('description').trim());
      }
      skill.save().then(() => {
        if (skill.get('validations.isValid')) {
          this.set('didValidate', false);
          this.set('newSkill', this.get('store').createRecord('skill'));
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new skill.', 'Success!');
        }
      });
    },
    clearFilter() {
      this.set('filter', null);
    },

    toggleWarningModal(skillId) {
      this.set('currentSkill', this.get('store').peekRecord('skill', skillId));
      this.toggleProperty('isShowingWarningModal');
    },

    deleteSkill() {
      this.get('currentSkill').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted skill.', 'Success!');
      });
    }
  },
});
