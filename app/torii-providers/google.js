import GoogleOauth2Provider from 'torii/providers/google-oauth2-bearer';

export default GoogleOauth2Provider.extend({
  fetch(data) {
    return data;
  }
});
