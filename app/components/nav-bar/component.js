import Ember from 'ember';

export default Ember.Component.extend({
  session: Ember.inject.service(),
  activeUser: Ember.computed('session.activeUser', function() {
    return this.get('session.activeUser');
  }),
  actions: {
    logout() {
      this.get('session').invalidate();
    }
  },
  didInsertElement() {
    $('#page-wrapper').css('min-height', $(window).height() + 'px');
  }
});
