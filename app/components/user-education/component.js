/* global toastr */
import Ember from 'ember';
const {
  computed
} = Ember;
import {
  EDUCATION_TYPE_LIST as educationTypes,
}
from './../../home/resources';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  isShowingModal: false,
  selectedEducation: null,
  selectedEducationType: null,
  educationTypeList: educationTypes,
  newUserEducation: null,
  didValidate: false,
  filteredEducationList: computed('educationList', 'selectedEducationType', function() {
    return this.get('educationList').filter(education => {
      return education.get('type').includes(this.get('selectedEducationType.name'));
    });
  }),
  init() {
    this._super(...arguments);
    this.set('newUserEducation', this.get('store').createRecord('user-education'));
  },
  actions: {
    toggleModal() {
      this.toggleProperty('isShowingModal');
      this.set('didValidate', false);
      this.set('selectedEducationType', null);
      this.set('selectedEducation', null);
      this.set('note', null);
      this.set('grade', null);
      this.set('newUserEducation', this.get('store').createRecord('user-education'));
    },
    addNewEducation(userId) {
      this.set('didValidate', true);
      let user = this.get('store').peekRecord('user', userId);
      let userEducation = this.get('newUserEducation');
      userEducation.set('grade', this.get('grade'));
      userEducation.set('note', this.get('note'));
      userEducation.set('startDate', userEducation.get('startDate'));
      userEducation.set('endDate', userEducation.get('endDate'));
      userEducation.set('education', this.get('selectedEducation'));
      userEducation.save().then((userEducation) => {
        if (userEducation.get('validations.isValid')) {
          this.set('didValidate', false);
          user.get('userEducations').pushObject(userEducation);
          this.set('selectedEducationType', null);
          this.set('selectedEducation', null);
          this.set('note', null);
          this.set('grade', null);
          this.set('newUserEducation', this.get('store').createRecord('user-education'));
          this.toggleProperty('isShowingModal');
          toastr.success('You have successfully added new education.', 'Success!');
        }
      });
    },
    selectChange(prop, val) {
      this.set(prop, val);
      if (prop === 'selectedEducationType') {
        this.set('selectedEducation', null);
      }
    },
    toggleWarningModal(id, name) {
      this.set('currentEducation', this.get('store').peekRecord('user-education', id));
      this.set('currentEducationName', name);
      this.toggleProperty('isShowingWarningModal');
    },
    deleteUserEducation() {
      this.get('currentEducation').destroyRecord().then(() => {
        this.toggleProperty('isShowingWarningModal');
        toastr.success('You have successfully deleted education.', 'Success!');
      });
    }
  }
});
