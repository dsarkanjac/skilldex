import Model from 'ember-data/model';
import attr from 'ember-data/attr';

import {
  validator,
  buildValidations
} from 'ember-cp-validations';

const Validations = buildValidations({
  projectName: validator('presence', true),
  client: validator('presence', true),
  role: validator('presence', true),
  startDate: validator('presence', true),
});

export default Model.extend(Validations, {
  projectName: attr('string'),
  client: attr('string'),
  role: attr('string'),
  startDate: attr('date'),
  endDate: attr('date'),
  description: attr('string')
});
