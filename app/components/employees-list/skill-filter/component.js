import Ember from 'ember';

export default Ember.Component.extend({
  grade: null,
  experience: null,
  skillName: null,
  userSkillsList: Ember.computed('userSkills', function() {
    return this.get('userSkills').mapBy('name').uniq();
  }),
  skillNameObs: Ember.observer('skillName', 'experience', 'grade', function() {
    if (this.get('selectedSkillList').findBy('id', this.get('rowId'))) {
      let obj = this.get('selectedSkillList').findBy('id', this.get('rowId'));
      obj.setProperties({
        'name': this.get('skillName'),
        'experience': this.get('experience'),
        'grade': this.get('grade')
      });
    } else {
      let obj = Ember.Object.create({
        id: this.get('rowId'),
        name: this.get('skillName'),
        experience: this.get('experience'),
        grade: this.get('grade')
      });

      this.get('selectedSkillList').addObject(obj);
    }
  }),
  actions: {
    selectChange(prop, val) {
      this.set(prop, val);
    },
    removeSkillName(id) {
      this.setProperties({
        'skillName': null,
        'grade': null,
        'experience': null
      });

      let list = this.get('selectedSkillList');
      list.removeObject(list.findBy('id', id));
    }
  }
});
